.. HERITAGE documentation master file, created by
   sphinx-quickstart on Wed Aug 20 17:23:23 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HERITAGE documentation!
==================================================

This documentation will guide you to creation of Text Adventure games in the 
HERITAGE game format.

HERITAGE is available on https://notabug.org/SylvieLorxu/HERITAGE.

This guide is licensed under the Creative Commons Attribution license. All 
examples are licensed under the Creative Commons Zero 1.0 license.

Contents:

.. toctree::
   :maxdepth: 2

   pages/introduction.rst
   pages/game_creation.rst
   pages/statements.rst
   pages/functions.rst
   pages/variables.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

