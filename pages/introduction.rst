============
Introduction
============

HERITAGE stands for *HERITAGE Equals Retro Interpreting Text Adventure Game 
Engine*.

It is both an engine (implemented in Javascript) and a text adventure game 
file format, readable by any application implementing the HERITAGE standard.

The HERITAGE file format is characterized by being simple to learn and write, 
yet being powerful for even more complicated text adventure games.
