==================
HERITAGE variables
==================

_game_over
----------

When read::

  Returns 0

When written to::

  If set to a non-zero value, ends the current game

_random
-------

When read::

  Returns random value from 1 through 100 (both inclusive)

When written to::

  Discards input

_turn
-----

When read::

  Returns current game turn

When written to::

  Discards input

_write_to
---------

When read::

  Returns the variable the next user input will write to, or 0 if no variable is defined

When written to::

  Will save the next user input to the name of the variable given

_yesno
------

When read::

  Returns either 0 or 1 (randomly chosen)

When written to::

  Discards input
