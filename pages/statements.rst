===================
HERITAGE statements
===================

action
------

Syntax::

  action(name):
      succeed:

Example::

  action(open_door):
      succeed: You open the door
      fail: You don't have a key
      require_inventory: key

action allows the developer to create specific commands that are not tied to a 
specific item and can always be executed (this specific example is better used 
as an on_open substatement on a door item).

name is the command the user will type. If it is more than one word, it should 
be seperated by an underscore.

action supports the following substatements::

  succeed: statement to print when the action succeeds
  fail: statement to print when the action fails
  require_location: locationnumber(X.Y.Z.) followed by a comma-seperated list of items that should be in that location
  require_here: comma-seperated list of items that should be on the player's current location
  require_inventory: comma-seperated list of items that should be in the player's inventory
  equals: check if variable is equal to value (variable, value)
  less_than: check if variable is less than a certain value (variable, value)
  more_than: check if variable is more than a certain value (variable, value)
  lose: comma-seperated list of items in inventory to disappear
  gain: comma-seperated list of items to be added to the inventory
  drop: comma-seperated list of items to be moved from the inventory to the current room

exit
----

Syntax::

  exit(id):

Example::

  exit(1):
      fail: The door is locked
      require_inventory: key

exit allows the developer to create special exits. These are exits that do not 
use a standard direction (north, east, south, west, northeast, southeast, 
northwest, southwest, up and down) or exits on a standard direction that have 
special requirements. id is an unique number. The same id should not be 
re-used for multiple exits.

exit supports the following substatements::

  succeed: statement to print when the action succeeds
  fail: statement to print when the action fails
  require_location: locationnumber(X.Y.Z.) followed by a comma-seperated list of items that should be in that location
  require_here: comma-seperated list of items that should be on the player's current location
  require_inventory: comma-seperated list of items that should be in the player's inventory
  equals: check if variable is equal to value (variable, value)
  less_than: check if variable is less than a certain value (variable, value)
  more_than: check if variable is more than a certain value (variable, value)
  lose: comma-seperated list of items in inventory to disappear
  gain: comma-seperated list of items to be added to the inventory
  drop: comma-seperated list of items to be moved from the inventory to the current room
  new_location: X.Y.Z value of the room this exit should take you to

info
----

Syntax::

  info():
      title:
      author:
      license:

Example::

  info():
      title: Your game name
      author: Your name
      license: Your preferred license

info allows the developer to store game-specific information such as title, 
author or license.

The title, author and license fields are required. The developer may add as 
many additional fields as they want, but it is up to the HERITAGE 
implementation if these are shown and how these are shown.

import
------

Syntax::

  import(filename)

Example::

  import(items)

import allows the developer to import other HERITAGE files in the current 
directory or a subdirectory. This is mostly to ease organising game data, but 
can also be used to import "libraries" adding a set of standard items. Do note 
that the statements in the imported file are inserted on the location the 
import statement is. If you want to overwrite imported items, make sure to do 
so AFTER the import statement.

For security and privacy reasons, only files in the current directory or a 
subdirectory may be imported.

item
----

Syntax::

  item(name.instance):

Example::

  item(dark_coat.2):
      on_examine: This is a dark coat.

name is the name of the item to create, instance is an name-unique number. 
itemname is required, and multi-word items should have the words separated by 
an underscore. instance is not required, unless you want to have multiple 
items with the same name; item(dark_coat) is valid syntax. An item and 
instance combination should only be defined once.

on_handler fields define what to do when an user types a command. In the 
example, the text "This is a dark coat" would be printed if the player types 
"look at dark coat".

room
----

Syntax::

  room(x.y.z):

Example::

  room(0.0.0):
    first_enter: This text is returned the first time someone enters this room
    description: Description text, shown on entering the room or looking
    items: pen, computer.2
    exits: portal.3, east

room allows the user to create a room on a 3 dimensional grid. The player 
always starts at the room with X, Y and Z positions 0 (room(0.0.0)).

Each x.y.z combination should only be defined once.

room accepts the following substatements::

  first_enter: The text to print upon entering this room for the first time
  description: Text shown upon entering the room (also first time if first_enter is not defined) or looking
  items: comma-seperated list of items in the room
  exits: comma-seperated list of exits in the room

var
---

Syntax::

  var(name, value)

Example::

  var(points, 0)

var initializes a variable with the given value. This can be either an integer 
(0, 2, 14, etc.), the name of another variable that has been defined before, 
or a string (do note to use "double quotes").
