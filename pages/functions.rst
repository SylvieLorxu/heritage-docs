==================
HERITAGE functions
==================

Calculate
---------

Syntax::

  @(variable operator value)@

Returns::

  Result of calculation

Example::

  @(points+10)@

Calculates the new value and returns it. This action is non-destructive (the
actual value of the variable is not changed).

Accepted operators:  + - / * %

Change
------

Syntax::

  #(variable operator value)#

Returns::

  Nothing

Example::

  #(points+10)#

Calculates the new value and sets the variable to match this value. This
action is destructive.

Accepted operators: = + - / * %

Comment
-------

Syntax::

  /* Text to comment out */

Returns::

  Nothing

Example::

  /* This text is commented out and
     will never be shown in any way */

Prevents text from being displayed in the interface, or interpreted. This is
the only way to add text to a .heritage file without it meaning anything.

Comments can be spread over multiple lines.

Conditional
-----------

Syntax::

  $(condition:variable,value;if_true|if_false)$
  $(condition:list,of,variables;if_true|if_false)$

Returns::

  if_true on true; if_false on false

Example::

  $(!equals:_turn,5;Print this on any but the fifth turn)$
  $(require_here:key,paper,note;Print this if the current location contains the items key, paper and note)$
  $(equals:_yesno,1;Print on random true|Print on random false)$

Checks if a condition returns true, and returns the result if it does.

If you need to use | in your sentence, enter it twice: ||

Accepted conditions: require_location, require_here, require_inventory, equals, less_than, more_than

Echo
----

Syntax::

  !(variable)!

Returns::

  Value of variable

Example::

  !(_turn)!

Prints the value of a variable.
